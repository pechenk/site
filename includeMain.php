﻿<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="bg.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="./js/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="./js/binary.js"></script><script type="text/javascript" src="./js/jquery.validate.min.js"></script>
<script type="text/javascript; charset=utf-8" src="my.js"></script>
</head>
<body>
<?php
include 'includeFSDB.php';
$user_info = get_user_info_FSDB();
if ($user_info['login']) {
	$tmplHeader = 'tmplHeaderMenuLoggedIn.php';
	$tmplProfileBlock = 'tmplProfileBlockLoggedIn.php';
	$tmplForms = 'tmplForms.php';
} else {
	$tmplHeader = 'tmplHeaderMenuGuest.php';
	$tmplProfileBlock = 'tmplProfileBlockGuest.php';
	$tmplForms = 'tmplForms.php';
}
?>